$Id 

Open Library API
================

This module is designed to be a set of simple wrapper functions for querying 
book edition and author metadata from http://openlibrary.org or a service 
running the same software (http://openlibrary.org/dev/docs/setup). It also 
transparently caches requests, to be kind to the providers of these services.

In addition a set of theme functions are provided, which for many developers 
may be all they need to achieve what they want.

There are also two sample modules that make use of the API.

Open Library Filter
===================

This is an input filter module which inserts book edition metadata into posts 
using a simple wiki-like syntax. eg.:

[[isbn:9781430209898]]

will insert the title of the book, linking to the edition's page on 
http://openlibrary.org (or equivalent service), and

[[isbn:9781430209898|block]]

will insert the book cover image (if it exists), and other book metatdata, 
depending on the theme template file used.

Open Library ISBN Formatter
===========================

This is a CCK display formatter for the ISBN Field module 
(http://drupal.org/project/isbn). It provides a 'title' or 'block' display 
format (as above) for ISBN fields.

Caution
=======

The Open Library API module is at an early stage of development, and future 
versions may change in ways which are not backwards-compatible. I know that as 
a Drupal developer you will be deeply shocked at the very thought, but don't 
say you haven't been warned.


API Examples
============

<?php
// Getting an Open Library ID from an ISBN number:
$args = array(
  'type' => '/type/edition', // you must specify a type
  'isbn_13' => '9781430209898', // all keys and values have to be strings
);
print_r(openlibrary_query($args));
?>

returns:

Array
(
    [0] => stdClass Object
        (
            [key] => /b/OL23226198M
        )

)


<?php
// Getting a resource from an Open Library ID:
print_r(openlibrary_get_content('/b/OL23226198M'));
?>

returns:

stdClass Object
(
    [series] => Array
        (
            [0] => Expert's voice in open source
            [1] => Books for professionals by professionals
        )

    [lc_classifications] => Array
        (
            [0] => TK5105.8885.D78 V36 2008
        )

    [latest_revision] => 1
    [id] => 34036026
    [languages] => Array
        (
            [0] => stdClass Object
                (
                    [key] => /l/eng
                )

        )

    [source_records] => Array
        (
            [0] => marc:marc_loc_updates/v37.i19.records.utf8:12979854:699
        )

    [title] => Pro Drupal development
    [edition_name] => 2nd ed.
    [...]
    

<?php
// Getting a resource history from an Open Library ID:
print_r(openlibrary_get_history('/b/OL23226198M'));
?>

returns:

Array
(
    [0] => stdClass Object
        (
            [comment] => initial import
            [author] => /user/ImportBot
            [ip] => 207.241.229.141
            [created] => 2009-05-16T18:56:28.658773
            [key] => /b/OL23226198M
            [action] => bulk_update
            [author_id] => 17874995
            [id] => 34230680
            [machine_comment] => 
            [revision] => 1
        )

)


Authors and editions are treated much the same in the OpenLibrary API.
<?php
// Getting an author:
openlibrary_get_content('/a/OL229501A');

// Getting an author's image:
openlibrary_get_cover('OL229501A', 'olid', 'a');
?>
